def score(word):
	one = "aeioulnrst"
	two = "dg"
	three = "bcmp"
	four = "fhvwy"
	five = "k"
	eight = "jx"
	ten = "qz"
	#word = "Cabbage"

	score = 0

	for ch in word:
		ch = ch.lower()
		if ch in one:
			score += 1  
		elif ch in two:
			score += 2 
		elif ch in three:
			score += 3 
		elif ch in four:
			score += 4 
		elif ch in five:
			score += 5 
		elif ch in eight:
			score += 8 
		elif ch in ten:
			score += 10
	return score
	pass
